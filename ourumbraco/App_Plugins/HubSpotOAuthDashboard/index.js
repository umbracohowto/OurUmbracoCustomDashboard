angular.module("umbraco").controller("HubSpotOAuthController", function ($scope, localizationService, $http) {
	//if (!$scope.model.value) {
	//	$scope.model.value = {
	//		client_id: "",
	//		scope: "content",
	//		client_secret: "",
	//		redirect_uri: "",
	//		authorized: false
	//	};
	//}
	//$scope.model.value.redirect_uri = window.location.origin + "/umbraco/surface/hubspotoauthapi/storecode";
	
	var localizeList = [
		"DashboardTabs_Authorize",
		"DashboardTabs_Authorized",
		"DashboardTabs_NotAuthorized",
		"DashboardTabs_ReadMe"
		];
	$scope.translations = {};

	localizationService.localizeMany(localizeList).then(function (data) {
		for (var i = 0; i < localizeList.length; ++i) {
			$scope.translations[localizeList[i]] = data[i];
		}
	});
	$scope.authorize = function () {
		$http.get(window.location.origin + "/umbraco/surface/hubspotoauthapi/savevalue?clientId=" + $scope.model.value.client_id + "&scopes=" + $scope.model.value.scope + "&clientSecret=" + $scope.model.value.client_secret + "&redirectUri=" + $scope.model.value.redirect_uri).then(function (response) {
			window.location = response.data;
		}, function (err) {
		});
	};
});